trollsift (0.5.3-1) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Bump Standards-Version to 4.7.0, no changes.

  [ Antonio Valentino ]
  * New upstream release.
  * debian/control:
    - Add dependency on pybuild-plugin-pyproject.
    - Switch to hatchling: add edpendency on python3-hatchling
      and python3-hatch-vcs.
    - Do not use undefined substitution variables.
  * debian/patches:
    - Drop 0001-Include-tests-sub-package.patch,
      no longer needed.
  * debian/copyright:
    - Drop entry for versioneer, no longer needed.
  * Update d/clean.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Tue, 03 Dec 2024 23:20:03 +0000

trollsift (0.5.1-2) unstable; urgency=medium

  * debian/control:
    - Drop dependency on python3-six (Closes: #1064868).
  * Update dates in d/copyright.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Mon, 26 Feb 2024 23:18:37 +0000

trollsift (0.5.1-1) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Team upload.
  * Enable Salsa CI.
  * Bump Standards-Version to 4.6.2, no changes.
  * Bump debhelper compat to 13.
  * Remove generated files in clean target.
  * Switch to dh-sequence-*.

  [ Antonio Valentino ]
  * New upstream release.
  * debian/control:
    - Use <!nocheck> marker.
    - Remove dependency from python3-six, no longer needed.
  * Switch to autopkgtest-pkg-pybuild and remove the
    no longer necessary d/tests folder.
  * debian/copyright:
    - Update copyright date.
    - Update versioneer license.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sat, 14 Oct 2023 07:02:54 +0000

trollsift (0.5.0-1) unstable; urgency=medium

  * New upstream release.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Wed, 23 Nov 2022 06:48:42 +0000

trollsift (0.4.0-2) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Bump Standards-Version to 4.6.1, no changes.
  * Use supported python3 versions in autopkgtest.

  [ Antonio Valentino ]
  * Fix d/copyright formatting.
  * Add test-dependency on python3-all to d/tests/control.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sat, 15 Oct 2022 17:47:39 +0000

trollsift (0.4.0-1) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Bump Standards-Version to 4.6.0, no changes.

  [ Antonio Valentino ]
  * New upstream release.
  * Update d/copyright.
  * debian/patches:
    - refresh all patches.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sat, 05 Feb 2022 08:25:33 +0000

trollsift (0.3.5-1) unstable; urgency=medium

  * Team upload.
  * Update watch file for GitHub URL changes.
  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 15 Aug 2021 17:29:42 +0200

trollsift (0.3.5-1~exp1) experimental; urgency=medium

  [ Bas Couwenberg ]
  * Bump Standards-Version to 4.5.1, no changes.

  [ Antonio Valentino ]
  * New upstream release.
  * debian/control:
    - add build-dependency on python3-pytest
  * debian/patches:
    - new 0001-Include-tests-sub-package.patch
  * Add autopkgtests.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Thu, 18 Feb 2021 07:54:55 +0100

trollsift (0.3.4-1) unstable; urgency=medium

  * New upstream release.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sun, 22 Dec 2019 08:11:07 +0000

trollsift (0.3.3-1) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Update gbp.conf to use --source-only-changes by default.
  * Bump Standards-Version to 4.4.1, no changes.

  [ Antonio Valentino ]
  * New upstream release.
  * Bump debhelper from old 11 to 12.
  * Remove obsolete fields Name from debian/upstream/metadata.
  * Explicitly set Rules-Requires-Root: no.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Fri, 11 Oct 2019 05:59:20 +0000

trollsift (0.3.2-1) unstable; urgency=medium

  [ Antonio Valentino ]
  * New upstream release.

  [ Bas Couwenberg ]
  * Add license & copyright for versioneer.py.

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Fri, 18 Jan 2019 07:29:18 +0000

trollsift (0.3.1-2) unstable; urgency=medium

  * Add missing attribution in debian/copyright (Closes: #917298).

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Tue, 25 Dec 2018 19:11:07 +0000

trollsift (0.3.1-1) unstable; urgency=medium

  * Initial version (Closes: #916541)

 -- Antonio Valentino <antonio.valentino@tiscali.it>  Sat, 15 Dec 2018 19:03:19 +0000
